export const experienceComponent = {
  template: `
  <div class="container mt-5">
    <div class="row">
      <div class="col-sm-4 mb-3">
        <div class="card border border-primary shadow">
          <div class="card-body text-center">
            <i class="fab fa-css3 fa-3x"></i>
            <h5 class="card-text mt-3">CSS3</h5>
            <p>1 Years Experience</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mb-3">
        <div class="card border border-primary shadow">
          <div class="card-body text-center">
            <i class="fab fa-bootstrap fa-3x"></i>
            <h5 class="card-text mt-3">Bootstrap</h5>
            <p>10 Months Experience</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mb-3">
        <div class="card border border-primary shadow">
          <div class="card-body text-center">
            <i class="fab fa-vuejs fa-3x"></i>
            <h5 class="card-text mt-3">Vue JS</h5>
            <p>2 Months Experience</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4 mb-3">
        <div class="card border border-primary shadow">
          <div class="card-body text-center">
            <i class="fab fa-php fa-3x"></i>
            <h5 class="card-text mt-3">PHP</h5>
            <p>8 Months Experience</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mb-3">
        <div class="card border border-primary shadow">
          <div class="card-body text-center">
            <i class="fab fa-free-code-camp fa-3x"></i>
            <h5 class="card-text mt-3">Codeigniter</h5>
            <p>6 Month Experience</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mb-3">
        <div class="card border border-primary shadow">
          <div class="card-body text-center">
            <i class="fas fa-database fa-3x"></i>
            <h5 class="card-text mt-3">MariaDB</h5>
            <p>8 Years Experience</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row mt-5">
      <h4 class="text-center">Work Experience</h4>
      <div class="col">
        <ul>
          <li><h5>PT Mangala Kreasi Indonesia</h5></li>
          <small>July 2020 - Current</small>
          <p>Junior Programmer</p>
          <li><h5>PT Chubb Safes Indonesia</h5></li>
          <small>April 2019 - July 2019</small>
          <p>Intern IT Support</p>
        </ul>
      </div>
    </div>
  </div>
  `
}