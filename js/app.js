import { experienceComponent } from "./experienceComponent.js";

let home = {
  data() {
    return {
      imageProfile: "image/profile.jpg"
    }
  },
  template: `
  <section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
        <img :src="imageProfile" class="img-fluid rounded-circle img-jumbotron">
        <h1 class="fw-light mt-3">Hello, I'm Sanjaya</h1>
        <p class="lead text-muted">Profesional Back End Developer</p>
        <div class="icon">
          <a class="text-muted mx-1" target="_blank" href="https://bitbucket.org/sanjaya411"><i class="fab fa-bitbucket fa-2x"></i></a>
          <a class="text-muted mx-1" target="_blank" href="https://facebook.com/muhammadricky.sanjaya.1/"><i class="fab fa-facebook fa-2x"></i></a>
          <a class="text-muted mx-1" target="_blank" href="https://www.instagram.com/sanjaya_411/"><i class="fab fa-instagram fa-2x"></i></a>
          <a class="text-muted mx-1" target="_blank" href="https://api.whatsapp.com/send/?phone=6281213490755"><i class="fab fa-whatsapp fa-2x"></i></a>
        </div>
      </div>
    </div>
  </section>
  `
}

let routes = [
  {path: "/", component: home},
  {path: "/experience", component: experienceComponent}
];

let router = new VueRouter({
  routes
})

Vue.component("navbar", {
  template: `
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <router-link to="/">
        <img src="image/navbar.png" class="img-navbar">
      </router-link>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <router-link class="nav-link " aria-current="page" to="/">Home</router-link>
          </li>
          <li class="nav-item">
          <router-link class="nav-link " aria-current="page" to="/experience">Experience</router-link>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  `
});


let vm = new Vue({
  el: "#app",
  data: {
    imageProfile: "image/profile.png"
  },
  router
})